# Chatbot Demo for #LTArena

## Présentation
Ce dépôt contient les ressources nécessaires au déploiement du chatbot présenté à #LTArena.
Le chatbot est construit sur Dialogflow. Il simule un chatbot déployé sur le site d'une boutique de vélo. Le chatbot permet de

1. Consulter les horaires du magasin
2. Consulter la liste des produits disponibles.
3. Consulter les créneaux disponibles pour un rendez-vous
4. Prendre un rendez vous pour un entretien ou une réparation.

Ce webhook prend en charge la partie 3. Quand un client veut prendre un rendez-vous, le chatbot interroge le webhook pour trouver un créneau disponible. Le Webhook prend les paramètres du rendez-vous, et si le créneau n'est pas disponible, il refuse le rendez-vous et affiche la liste des créneaux disponibles. Si le rendez-vous est valable, le webhook invite l'utilisateur à rentrer son adresse email. Le webhook inscrit le rendez-vous dans l'agenda du magasin et permet ajoute le client comme participant grâce à son adresse email.

## Déploiement

1. Créez un compte Dialogflow.
2. Créez un agent.
3. Créez un intent appelé **reservation_velo** qui doit renvoyer une date, une heure, un type de service, et une adresse email, respectivement de nom

  - date,
  - time,
  - type_service,
  - email
  
4. Créez les clés pour utiliser google calendar : https://developers.google.com/calendar/quickstart/python
5. Mettez les fichiers credentials.json et token.json à la racine du projet.
6. Installez docker, et exécutez, depuis la racine du projet, les commandes
7. `docker-compose build` puis `docker-compose up`
8. Téléchargez ngrok et exécutez `./ngrok http localhost:8000` pour exposer votre webhook sur les ports 80 et 443.
9. Renseignez l'adresse https://xxxxx.ngrok.io/webhook dans le chamb webhook de l'onglet fulfillment de votre agent Dialogflow, et activez le Webhook
10. activez le webhook pour l'intent
11. Votre agent dialogflow a maintenant accès à votre webhook.


# Ressources Présentation

ELIZA en français : https://elizia.net/

la discussion entre PARRY et ELIZA : https://tools.ietf.org/pdf/rfc439.pdf

Jabberwacky : http://www.jabberwacky.com/chat-george

Technologies derrière ALICE : https://www.pandorabots.com/pandora/pics/wallaceaimltutorial.html, https://code.google.com/archive/p/aiml-en-us-foundation-alice/

https://gist.github.com/onlurking/f6431e672cfa202c09a7c7cf92ac8a8b

... to continue
