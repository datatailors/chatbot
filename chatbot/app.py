from flask import Flask, request
from chatbot.utils.hooks import inventaire, reservation


def create_app():
    """
    Create a Flask application using the app factory pattern.

    :return: Flask app
    """
    app = Flask(__name__, instance_relative_config=True)

    app.config.from_object('config.settings')
    app.config.from_pyfile('settings.py', silent=True)

    @app.route('/webhook', methods=['POST'])
    def webhook():
        data = request.get_json(silent=True)
        intent = data['queryResult']['intent']['displayName']

        if intent == 'inventaire':
            response = inventaire(data)

        elif intent == 'reservation_velo':
            calid = app.config['CAL_ID']
            response = reservation(data, calid)

        return response

    return app
