import datetime
from dateutil.parser import parse
from datetime import timedelta


def create_event(service, calID, type_service, horaire, mail):

    event = {
        'summary': 'Rendez-vous pour vélo',
        'location': 'La cantine numérique, 18 bis Place Parmentier, 80000 Amiens, France',
        'description': type_service,
        'start': {
            'dateTime': horaire.isoformat(),
            'timeZone': 'Europe/Paris',
        },
        'end': {
            'dateTime': (horaire + timedelta(hours=1)).isoformat(),
            'timeZone': 'Europe/Paris',
        },
        'attendees': [
            {'email': mail},
        ],
    }

    event = service.events().insert(calendarId=calID, body=event, sendNotifications=True).execute()
    link = event.get('htmlLink')
    print("event created".format(link))
    return


def get_free_slots(service, calID, a=None):
    if a is None:
        a = datetime.datetime.today().astimezone()
        print(a)

    a = roundTime(a, 3600)
    print(a)  # round to the hour
    busy_frame = freebusy(service, calID)
    busy_frame_list = busy_frame[calID]['busy']

    busy_slots = []
    for i in range(0, len(busy_frame_list)):
        start_dt = parse(busy_frame_list[i]['start'])
        end_dt = parse(busy_frame_list[i]['end'])
        for dt in daterange(start_dt, end_dt):
            busy_slots.append(dt)
            #print(dt)

    dateList = []
    for x in range(0, 7 * 24):
        new_frame = a + datetime.timedelta(hours=x)
        # print(new_frame.astimezone())
        if (new_frame.hour >= 9 and new_frame.hour < 17) and new_frame.weekday() < 5:
            if not new_frame.astimezone() in busy_slots:
                dateList.append(new_frame.astimezone())

    return dateList


def freebusy(service, calID):
    the_datetime = datetime.datetime.now(datetime.timezone.utc)
    the_datetime2 = the_datetime + datetime.timedelta(days=7)
    body = {
        "timeMin": the_datetime.astimezone().isoformat(),
        "timeMax": the_datetime2.astimezone().isoformat(),
        "timeZone": "Europe/Paris",
        "items": [{"id": calID, }, ]
    }

    eventsResult = service.freebusy().query(body=body).execute()
    cal_dict = eventsResult[u'calendars']
    for cal_name in cal_dict:
        print(cal_name, cal_dict[cal_name])
    return cal_dict


def roundTime(dt=None, roundTo=60):
    """Round a datetime object to any time lapse in seconds
    dt : datetime.datetime object, default now.
    roundTo : Closest number of seconds to round to, default 1 minute.
    """
    if dt is None:
        dt = datetime.datetime.now()
    seconds = (dt.replace(tzinfo=None) - dt.min).seconds
    rounding = (seconds + roundTo / 2) // roundTo * roundTo
    return dt + datetime.timedelta(0, rounding - seconds, -dt.microsecond)


def daterange(date1, date2):
    for n in range(int((date2 - date1).seconds / 3600)):
        yield date1 + timedelta(hours=n)


def beautify_slots(free_slots):
    week_days = {
        0: "Lundi",
        1: "Mardi",
        2: "Mercredi",
        3: "Jeudi",
        4: "Vendredi",
        5: "Samedi",
        6: "Dimanche"
    }
    slot_pretty = []
    for slot in free_slots:
        text = """{0} {1}/{2} : {3}h""".format(week_days[slot.weekday()],
                                               slot.day, slot.month, slot.hour + 2)
        slot_pretty.append(text)
    return slot_pretty
