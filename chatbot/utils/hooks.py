from flask import jsonify
from oauth2client import file, client, tools
from googleapiclient.discovery import build
from chatbot.utils.calutils import get_free_slots, create_event, beautify_slots
from httplib2 import Http
from dateutil.parser import parse
import datetime


def inventaire(data):

    piece = data['queryResult']['parameters']['piece']
    inventaire = {
        "guidon": 4,
        "velo": 2,
        "chaine": 12,
        "cadre": 6,
        "pedale": 3
    }

    if piece not in inventaire:
        response = "Cette pièce n'est pas présente dans notre inventaire"

    else:
        response = """
            Il reste {0} {1}(s)
            """.format(inventaire[piece], piece)

    reply = {"fulfillmentText": response, }
    return jsonify(reply)


def reservation(data, calID):
    req_time = parse(data['queryResult']['parameters']['time']).time()
    req_date = parse(data['queryResult']['parameters']['date']).date()
    print("req_date : {0}".format(req_date.isoformat()))
    print("req_time : {0}".format(req_time.isoformat()))
    print("Hour : {0}".format(req_time.hour))

    horaire = datetime.datetime(req_date.year, req_date.month, req_date.day, req_time.hour-2).astimezone(
        tz=datetime.timezone(datetime.timedelta(0, 7200), 'CEST'))

    print("horaire demandé : {0}".format(horaire.isoformat()))
    type_service = data['queryResult']['parameters']['type_service']
    mail = data['queryResult']['parameters']['email']

    store = file.Storage('instance/token.json')
    creds = store.get()
    SCOPES = 'https://www.googleapis.com/auth/calendar'
    if not creds or creds.invalid:
        flow = client.flow_from_clientsecrets(
            'instance/credentials.json', SCOPES)
        creds = tools.run_flow(flow, store)
    service = build('calendar', 'v3', http=creds.authorize(Http()))

    calID = calID
    week_days = {
        0: "Lundi",
        1: "Mardi",
        2: "Mercredi",
        3: "Jeudi",
        4: "Vendredi",
        5: "Samedi",
        6: "Dimanche"
    }

    horaire_now = datetime.datetime.today().astimezone(
        tz=datetime.timezone(datetime.timedelta(0, 7200), 'CEST'))
    print("horaire now : {0}".format(horaire_now))
    free_slots = get_free_slots(calID=calID, service=service, a=horaire_now)
    # for slot in free_slots:
    #     print(slot)

    print("horaire in free slot: {0}".format(horaire in free_slots))
    if horaire in free_slots:
        create_event(service=service, calID=calID, type_service=type_service,
                     horaire=horaire, mail=mail)
        response = """
        Un rendez-vous pour {0} a été programmé le {1} {2}/{3} à {4}h.
        Un email de confirmation a été envoyé à {5}.
        """.format(type_service, week_days[horaire.weekday()], horaire.day,
                   horaire.month, horaire.hour, mail)
    else:
        pretty_slots = "\n".join(beautify_slots(free_slots))
        response = """
        Le créneau demandé n'est pas disponible. Places restantes :
        {0}
        """.format(pretty_slots)

    reply = {"fulfillmentText": response, }
    return jsonify(reply)
