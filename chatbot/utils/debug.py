from dateutil.parser import parse
import datetime
from calutils import freebusy, roundTime


def get_free_slots(service, calID, a=None):
    # check available hours fo rthe next 7 days
    if a is None:
        a = datetime.datetime.today()

    a = roundTime(a, 3600)
    busy_frame = freebusy(service, calID)
    busy_frame_list = busy_frame[calID]['busy']

    busy_slots = []
    for i in range(0, len(busy_frame_list)):
        start_dt = parse(busy_frame_list[i]['start'])
        end_dt = parse(busy_frame_list[i]['end'])
        for dt in daterange(start_dt, end_dt):
            busy_slots.append(dt)

    dateList = []
    for x in range(0, 7 * 24):
        new_frame = a + datetime.timedelta(hours=x)
        if (new_frame.hour >= 9 and new_frame.hour < 17) and new_frame.weekday() < 6:
            if not parse(new_frame.astimezone().isoformat()) in busy_slots:
                dateList.append(new_frame)

    return dateList


def daterange(date1, date2):
    for n in range(int((date2 - date1).seconds / 3600)):
        yield date1 + timedelta(hours=n)
